import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { NgModule } from '@angular/core';
import { LoginComponent } from './common/auth/login.component';
import { SignUpComponent } from './common/auth/sign-up.component';
import { ToDoInfoComponent } from './to-do/info/to-do-info.component';
import { ToDoListComponent } from './to-do/list/to-do-list.component';
import { EventsComponent } from './events/events.component';
import { AddEventComponent } from './add-event/add-event.component';
import { EventInfoComponent } from './event-info/event-info.component';

const appRoutes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'sign-up', component: SignUpComponent },
  { path: 'todo/:todoId', component: ToDoInfoComponent },
  { path: 'todo', component: ToDoListComponent },
  { path: 'events', component: EventsComponent },
  { path: 'add-event', component: AddEventComponent },
  { path: 'event-info/:eventId', component: EventInfoComponent },
  { path: '**', component: HomeComponent },
];

@NgModule({
  exports: [RouterModule],
  imports: [RouterModule.forRoot(appRoutes)],
})
export class AppRoutingModule {}
