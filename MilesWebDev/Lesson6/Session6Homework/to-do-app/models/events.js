'use strict';
module.exports = (sequelize, DataTypes) => {
  var events = sequelize.define('events', {
    name: DataTypes.STRING,
    description: DataTypes.STRING,
    startTime: DataTypes.DATE,
    endTime: DataTypes.DATE,
    userId: DataTypes.INTEGER,
    location: DataTypes.STRING,

  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
        models.Events.belongsTo(models.UserRoles)
      }
    }
  });
  return events;
};