import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';


/*Here we use reactive forms so all the validation occurs here as opposed to the html document.*/
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  genders = ['male', 'female'];
  signupForm: FormGroup;
  forbiddenUsernames = ['Chris', 'Anna'];

  ngOnInit() {
    this.signupForm = new FormGroup({ /*controls are nothing more than key value pairs */
        'userData': new FormGroup({
        'username': new FormControl(null, [Validators.required, this.forbiddenNames.bind(this)]),
        'email': new FormControl(null, [Validators.required, Validators.email])
        }),
        'gender': new FormControl('male'),
        'hobbies': new FormArray([])
     });
  }

  onSubmit() {
    console.log(this.signupForm);
  }

  onAddHobby() {
    const control = new FormControl(null, Validators.required);
    (<FormArray>this.signupForm.get('hobbies')).push(control);
  }

  forbiddenNames(controls: FormControl): {[s: string]: boolean} {
    if (this.forbiddenUsernames.indexOf(controls.value)) {
      return {'nameIsForbidden': true};
    }
    return null;
  }
}
