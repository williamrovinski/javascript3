/*import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  @ViewChild('f') signupForm: NgForm;     @ViewChild is a component you can assign to store f in. 
                                            You then pass a variable signupForm which takes NgForm. 
  defaultQuestion = 'teacher';            /*In order to make full use of the databinding input ngModel, 
                                      you must declare the variable here and set it equal to a string. of course here its a default string
  answer = '';
  genders = ['male', 'female'];
  user = {
    username: '',
    email: '',
    secretQuestion: '',
    answer: '',
    gender: ''
  };
  submitted = false;

  suggestUserName() {
    const suggestedName = 'Superuser';
    this.signupForm.setValue({      setValue() is a method which can set any value for a particular javascript object, 
                                      we pass a javascript object in there by using what we got in our console log 
                                      as a template. 
      userData: {
        username: suggestedName,
        email: ''
      },
      secret: 'pet',
      questionAnswer: '',
      gender: 'male'
    });
    this.signupForm.form.patchValue({               Patches the value of the FormGroup. It accepts an object with control names as keys, 
                                                    and does its best to match the values to the correct controls in the group. We can 
                                                    use a similiar approach to building a javascript object using the console log layout.
      userData: {
        username: suggestedName
      }
    });
  }

  onSubmit(form: NgForm) {    NgForm is a module imported from angular, which creates an object in the way of a form, now NgForm 
                                and ngForm the one you passed are totally different entities and the only way its related is that 
                                the argument you passed is that it exists inside the form html element itself. It will bind it in a way
    console.log(form);
  }

  onSubmit() {
    /*console.log(this.signupForm);     Simply pass the variable here with a this. constructor to make it an object. 

    this.submitted = true;
    this.user.username = this.signupForm.value.userData.username;     All of these values these objects are equal to comes from what
                                                                      the names are on the app.component.html, Also we sometimes use 
                                                                      userData because for those variables, they originally come from 
                                                                      that javascript object. If you get confused look for the 
                                                                      div elements.
    this.user.email = this.signupForm.value.userData.email;
    this.user.secretQuestion = this.signupForm.value.secret;
    this.user.answer = this.signupForm.value.questionAnswer;
    this.user.gender = this.signupForm.value.gender;

    this.signupForm.reset();
  }
}*/

import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
    subscriptions = ['Basic', 'Advanced', 'Pro'];
    selectedSubscription = 'Advanced';
    @ViewChild('signupForm') sgnForm: NgForm;
    onSubmit() {
      console.log(this.sgnForm.value);
    }
}
